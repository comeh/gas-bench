[[_TOC_]]

# gas-bench
This project helps to run the `tezos-snoop` benchmarks for the Octez Tezos node : https://gitlab.com/tezos/tezos 

* Tezos-node octez documentation : http://tezos.gitlab.io/
* Tezos-node octez code repository : https://gitlab.com/tezos/tezos 

## Pre-requesites :

* Locally :

```
export PATH=".:$PATH"
```
* make --version 
`GNU Make 4.2.1`


## Distribute `tezos-snoop` benchmarks.
* [make/multi-snoop.mk](make/multi-snoop.mk) : get a list tezos benchmarks corresponding to the given tags, distribute their run to set of hosts where `tezos-snoop` is  available, and then gather the output in a local directory.


This script gets a list of tezos-snoop benchmarks names for a list of tags, and for each `tezos_benchmark_name` runs the following command, distributing it over a set of availble hosts, making sure that only 1 benchmark is run at a time on a each host, and without waiting other benchmarks to finish to run the next available ones from the list.
`./tezos-snoop benchmark <tezos_benchmark_name> and save to tmp/out/<tezos_benchmark_name>.workload --determinizer percentile@50 --bench-num 30 --nsamples 300 --seed 87612786 --dump-csv tmp/out/<tezos_benchmark_name>.csv `

### customize or override :pencil:
* [make/multi-snoop.mk](make/multi-snoop.mk) : edit the following variables according to your values

* Benchmark tags 
```
# Tags of benchmarks to run
tags ?= dummy
```
This will get the benchmarks returned by `tezos-snoop list benchmarks with tags any of $(tags)`
It will run on the first host of the list, before starting the benchmark runs.

* Hosts and tezos-snoop path 
```
# List of hosts where to run (you need to be able to ssh to each)
hosts := server-dev.umamiwallet.com dev-api.umamiwallet.com
```
List of hosts where to run the benchmarks.

```
# on the distant server, where is the tezos-snoop executable (assuming the same on all $(hosts) )
distpath   = /opt/tezos/tezos/
```
Path to the `tezos-snoop` executable. 

Assuming to find `$(distpath)/tezos-snoop` on all servers from the `$(hosts)` list.

### Launch :rocket:
Once the customizations have been made, run with `N ≤ number of hosts` :
```
./multi-snoop.mk run
```

```
$ ./multi-snoop.mk run -n
[...]
multi-snoop.mk clean; multi-snoop.mk stone; time multi-snoop.mk -j2;
```
### Launch to compare

```
$ ./multi-snoop.mk compare -n
[...]
multi-snoop.mk clean; time multi-snoop.mk; multi-snoop.mk stone; time multi-snoop.mk -j2;
```


### Outputs
* Benchmarks outputs for `dummy` tags 
```
$ ls -R tmp/out/
tmp/out/:
Dummy_1        Dummy_16_out  Dummy_23_out  Dummy_30_out  Dummy_38_out  Dummy_45_out  Dummy_52_out  Dummy_6       Dummy_67_out  Dummy_74_out  Dummy_81_out  Dummy_89_out  Dummy_96_out
Dummy_10       Dummy_17      Dummy_24      Dummy_31      Dummy_39      Dummy_46      Dummy_53      Dummy_60      Dummy_68      Dummy_75      Dummy_82      Dummy_8_out   Dummy_97
Dummy_100      Dummy_17_out  Dummy_24_out  Dummy_31_out  Dummy_39_out  Dummy_46_out  Dummy_53_out  Dummy_60_out  Dummy_68_out  Dummy_75_out  Dummy_82_out  Dummy_9       Dummy_97_out
Dummy_100_out  Dummy_18      Dummy_25      Dummy_32      Dummy_3_out   Dummy_47      Dummy_54      Dummy_61      Dummy_69      Dummy_76      Dummy_83      Dummy_90      Dummy_98
Dummy_10_out   Dummy_18_out  Dummy_25_out  Dummy_32_out  Dummy_4       Dummy_47_out  Dummy_54_out  Dummy_61_out  Dummy_69_out  Dummy_76_out  Dummy_83_out  Dummy_90_out  Dummy_98_out
Dummy_11       Dummy_19      Dummy_26      Dummy_33      Dummy_40      Dummy_48      Dummy_55      Dummy_62      Dummy_6_out   Dummy_77      Dummy_84      Dummy_91      Dummy_99
Dummy_11_out   Dummy_19_out  Dummy_26_out  Dummy_33_out  Dummy_40_out  Dummy_48_out  Dummy_55_out  Dummy_62_out  Dummy_7       Dummy_77_out  Dummy_84_out  Dummy_91_out  Dummy_99_out
Dummy_12       Dummy_1_out   Dummy_27      Dummy_34      Dummy_41      Dummy_49      Dummy_56      Dummy_63      Dummy_70      Dummy_78      Dummy_85      Dummy_92      Dummy_9_out
Dummy_12_out   Dummy_2       Dummy_27_out  Dummy_34_out  Dummy_41_out  Dummy_49_out  Dummy_56_out  Dummy_63_out  Dummy_70_out  Dummy_78_out  Dummy_85_out  Dummy_92_out
Dummy_13       Dummy_20      Dummy_28      Dummy_35      Dummy_42      Dummy_4_out   Dummy_57      Dummy_64      Dummy_71      Dummy_79      Dummy_86      Dummy_93
Dummy_13_out   Dummy_20_out  Dummy_28_out  Dummy_35_out  Dummy_42_out  Dummy_5       Dummy_57_out  Dummy_64_out  Dummy_71_out  Dummy_79_out  Dummy_86_out  Dummy_93_out
Dummy_14       Dummy_21      Dummy_29      Dummy_36      Dummy_43      Dummy_50      Dummy_58      Dummy_65      Dummy_72      Dummy_7_out   Dummy_87      Dummy_94
Dummy_14_out   Dummy_21_out  Dummy_29_out  Dummy_36_out  Dummy_43_out  Dummy_50_out  Dummy_58_out  Dummy_65_out  Dummy_72_out  Dummy_8       Dummy_87_out  Dummy_94_out
Dummy_15       Dummy_22      Dummy_2_out   Dummy_37      Dummy_44      Dummy_51      Dummy_59      Dummy_66      Dummy_73      Dummy_80      Dummy_88      Dummy_95
Dummy_15_out   Dummy_22_out  Dummy_3       Dummy_37_out  Dummy_44_out  Dummy_51_out  Dummy_59_out  Dummy_66_out  Dummy_73_out  Dummy_80_out  Dummy_88_out  Dummy_95_out
Dummy_16       Dummy_23      Dummy_30      Dummy_38      Dummy_45      Dummy_52      Dummy_5_out   Dummy_67      Dummy_74      Dummy_81      Dummy_89      Dummy_96

tmp/out/Dummy_100_out:
Dummy_100.csv  Dummy_100.workload

tmp/out/Dummy_10_out:
Dummy_10.csv  Dummy_10.workload

tmp/out/Dummy_11_out:
Dummy_11.csv  Dummy_11.workload

tmp/out/Dummy_12_out:
Dummy_12.csv  Dummy_12.workload

[...]
```

* Command outputs for `dummy` tags 
```
$ ./multi-snoop.mk run
Model N_IOpt_map_alpha already registered for code generation! (overloaded instruction?) Ignoring.
Model N_KIter_alpha already registered for code generation! (overloaded instruction?) Ignoring.
Model N_KList_enter_body_alpha already registered for code generation! (overloaded instruction?) Ignoring.
Model N_KMap_enter_body_alpha already registered for code generation! (overloaded instruction?) Ignoring.
multi-snoop.mk clean; multi-snoop.mk stone; time multi-snoop.mk -j2;
[...]
Dummy_99 running on dev-api.umamiwallet.com
Model N_IOpt_map_alpha already registered for code generation! (overloaded instruction?) Ignoring.
Model N_KIter_alpha already registered for code generation! (overloaded instruction?) Ignoring.
Model N_KList_enter_body_alpha already registered for code generation! (overloaded instruction?) Ignoring.
Model N_KMap_enter_body_alpha already registered for code generation! (overloaded instruction?) Ignoring.
Benchmarking with the following options:
{ options = { flush_cache=false;
              stabilize_gc=false;
              seed=87612786;
              bench #=30;
              nsamples/bench=300;
              determinizer=percentile 50;
              cpu_affinity=none;
              minor_heap_size=262144 words;
              config directory=None };
   save_file = /tmp/_snoop/benchmark_results/Dummy_99.workload;
   storage = Mem }
Using default configuration for benchmark Dummy_99
{}
benchmarking 30/30
stats over all benchmarks: { max_time = 26.000000 ; min_time = 24.000000 ; mean_time = 24.533333 ; sigma = 0.561743 }
Exporting to /tmp/_snoop/benchmark_results/Dummy_99.csv
Dummy_99 1643214374 dev-api.umamiwallet.com
real	2m18.494s
user	0m3.276s
sys	0m0.961s
```


# `tezos-snoop` patch for `dummy` tag
* cf. [./dummy-tag/](./dummy-tag/)
