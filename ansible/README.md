# Unsuccessful attempt with ansible  - do not use

Not working since ansible options `stragegy:free` xor `run_once:true` :

```
[WARNING]: Using run_once with the free strategy is not currently supported. This task will still be executed for every host in
the inventory list.
```
playbook KO :
```
---
- hosts: all
  strategy: free
  gather_facts: false

  tasks:

  - debug:
      msg: "Test"
    run_once: True
```

To review in case evolution happens on ansible around :
* https://github.com/ansible/ansible/issues/72747
* https://github.com/ansible/ansible/issues/15876
* [...]
