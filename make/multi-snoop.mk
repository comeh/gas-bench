#!/usr/bin/env -S make -Orecurse --no-print-directory -f

# Tags of benchmarks to run
tags ?= dummy

# List of hosts where to run
hosts := server-dev.umamiwallet.com dev-api.umamiwallet.com

# on the distant server, where is the tezos-snoop executable (assuming the same on all $(hosts) )
distpath   = /opt/tezos/tezos/


### end of customization

MAKEFLAGS += -Rr --warn-undefined-variables
SHELL != which bash
.SHELLFLAGS := -euo pipefail -c

.ONESHELL:
.DELETE_ON_ERROR:
.PHONY: phony
.DEFAULT_GOAL := main

self := $(lastword $(MAKEFILE_LIST))
$(self):;

tmp := tmp
out := $(tmp)/out
lock := $(tmp)/$(self).lock
next := $(tmp)/host
stone := $(tmp)/stone

# on the distant server, where to write the temporary files <bench>.{csv,workload} :
distoutdir = /tmp/_snoop/benchmark_results

# Generate rotational sequence of host from $(host) with index in file $(next)
host: $(next); @ n=$$(cat $<); echo $(hosts) | fmt -1 | sed -n $${n}p; echo "$$n % $(words $(hosts)) + 1" | bc -q | sponge $<
$(next): | $(tmp); @ echo 1 > $@
$(tmp):; @ mkdir -p $@

# Lock protect concurrent acces to host sequence generator
host := flock $(lock) $(self) host

# Connect to the first host and get the list of benchmarks with the $(tags)
snoops != ssh $(word 1, $(hosts)) $(distpath)/tezos-snoop list benchmarks with tags any of $(tags) | awk -F':' '{printf("%s ", $$1); next }'

# (Test) print the list of benchmarks found with those tags
bench_list:
	echo $(snoops)

# /opt/tezos/tezos/tezos-snoop benchmark $${1} and save to /tmp/_snoop/benchmark_results/$${1}.wo/rkload --determinizer percentile@50 --bench-num 300 --nsamples 3000 --seed 87612786 --dump-csv /tmp/_snoop/benchmark_results/$${1}.csv ;

# The pseudo task we distribute on $(hosts) nodes
snoop := snoop () { echo -n $${1:?} $$(date +%s) $$(hostname) ""; mkdir -p $(distoutdir) ; $(distpath)/tezos-snoop benchmark $${1} and save to $(distoutdir)/$${1}.workload --determinizer percentile@50 --bench-num 30 --nsamples 300 --seed 87612786 --dump-csv $(distoutdir)/$${1}.csv ; }


# Drive the concurrent calls to snoop from list snoops, on all remote nodes
$(out)/%: $(stone) | $(out);
	@ mkdir -p $(out)/$*_out; running_on=$$($(host)); echo $* "running on $${running_on}"
	+@ $(snoop); { declare -f snoop; echo snoop $(@F) ; } | ssh $${running_on} bash >> $@ ; tail -1 $@
	scp $${running_on}:$(distoutdir)/$*.{csv,workload} $(out)/$*_out 
$(out):; @ mkdir -p $@ 
$(stone): | $(tmp); @ touch $@


# sponge - sponge reads standard input and writes it out to the specified file. Unlike a shell redirect, sponge soaks up all its input before writing the output file. This allows constructing pipelines that read from and write to the same file.
sponge := /usr/bin/sponge
#sponge := /usr/local/bin/sponge
$(sponge):; $(error ERROR : sponge missing "sudo apt install moreutils")


main: phony $(sponge) $(snoops:%=$(out)/%)
clean: phony; @ rm -rf $(tmp)
stone: phony | $(tmp); @ touch $(stone)

## clean and run 
~ := run
$~: $~ := $(self) clean;
$~: $~ += $(self) stone;
$~: $~ += time $(self) -j$(words $(hosts));
$~:; $($@)

## compare outputs with and without parallelism 
~ := compare
$~: $~ := $(self) clean;
$~: $~ += time $(self);
$~: $~ += $(self) stone;
$~: $~ += time $(self) -j$(words $(hosts));
$~:; $($@)


## Author : @comeh 2022
