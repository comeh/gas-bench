#!/usr/bin/make -f

# To run on mac with local install of GNU Make 4.2.1 :
#!/usr/local/bin/gmake -f

MAKEFLAGS += -Rr --warn-undefined-variables
SHELL != which bash
.SHELLFLAGS := -euo pipefail -c

.ONESHELL:
.DELETE_ON_ERROR:
.PHONY: phony
.DEFAULT_GOAL := main

tmp := tmp/snoop
# on the distant server, where is the tezos-snoop executable (assuming the same on all $(hosts) )
distpath   = /opt/tezos/tezos/
# on the distant server, where to write the temporary files <bench>.{csv,workload} :
distoutdir = /tmp/_snoop/benchmark_results


hosts := server-dev.umamiwallet.com dev-api.umamiwallet.com

hosts_avail := $$(while [[ -z `for i in $(hosts); do if ! pgrep -f "ssh $i" -c &>/dev/null ; then echo -n "$i "; fi ; done` ]] ; do echo "no host available"; sleep 2; done )
host := $$(shuf -n 1 -e $(hosts_avail))


run := run () { mkdir -p $(distoutdir) ; $(distpath)/tezos-snoop benchmark $${1} and save to $(distoutdir)/$${1}.workload --determinizer percentile@50 --bench-num 30 --nsamples 300 --seed 87612786 --dump-csv $(distoutdir)/$${1}.csv ; }
# /opt/tezos/tezos/tezos-snoop benchmark $${1} and save to /tmp/_snoop/benchmark_results/$${1}.wo/rkload --determinizer percentile@50 --bench-num 300 --nsamples 3000 --seed 87612786 --dump-csv /tmp/_snoop/benchmark_results/$${1}.csv ;

$(tmp)/%: | $(tmp); @ echo $* "(running on $(host) )"; mkdir -p $(tmp)/$*_out ; $(run); { declare -f run; echo run $*; } | ssh $(host) bash > $@ ; scp $(host):$(distoutdir)/$*.{csv,workload} $(tmp)/$*_out
$(tmp):; mkdir -p $@

tags ?= dummy
# Get the benchmark list locally...
# blist != /opt/tezos/tezos/tezos-snoop list benchmarks with tags any of $(tags) | awk -F':' '{printf("%s ", $$1); next }'
# Get the benchmark list from first host (i.e. you don't need tezos-snoop locally, only on the benchmark host)
blist != ssh $(word 1, $(hosts)) $(distpath)/tezos-snoop list benchmarks with tags any of $(tags) | awk -F':' '{printf("%s ", $$1); next }'

main: phony $(blist:%=$(tmp)/%); @ echo; ls -l $(tmp)/*

clean: phony; rm -r $(tmp)
