
# get tezos/tezos 
`git clone git@gitlab.com:tezos/tezos.git`

# apply patch to create dummy tags
(from Lucas R.)

Get [./lucas-diff.diff](./lucas-diff.diff) and apply it.

```
cat > lucas-diff.diff
git apply lucas-diff.diff
make
```

You now can run `tezos-snoop list benchmarks with tags any of dummy`
